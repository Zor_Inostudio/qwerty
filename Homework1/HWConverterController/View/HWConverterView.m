//
//  HWConverterView.m
//  Homework1
//
//  Created by Vladislav Grigoriev on 29/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#import "HWConverterView.h"
#import "HWRoundRectButton.h"
#import "HWNumericTextField.h"

@implementation HWConverterView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];

        _contentInsets = UIEdgeInsetsMake(HWDefaultMargin,
                                          HWDefaultMargin,
                                          HWDefaultMargin,
                                          HWDefaultMargin);
        
        _inputTextField = [[HWNumericTextField alloc] init];
        _inputTextField.placeholder = NSLocalizedString(@"converter.view.input.placeholder", nil);
        _inputTextField.keyboardType = UIKeyboardTypeDecimalPad;
        [self addSubview:_inputTextField];
        
        _convertButton = [[HWRoundRectButton alloc] init];
        [_convertButton setTitle:NSLocalizedString(@"converter.view.button.title", nil) forState:UIControlStateNormal];
        [self addSubview:_convertButton];
        
        _resultLabel = [[UILabel alloc] init];
        _resultLabel.numberOfLines = 0;
        _resultLabel.font = [UIFont systemFontOfSize:16.0f];
        [self addSubview:_resultLabel];
    }
    return self;
}

#pragma mark - Accessors

- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
    [self setNeedsLayout];
}

#pragma mark - Overrite

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect actualBounds = UIEdgeInsetsInsetRect(self.bounds, self.contentInsets);
    CGFloat inputHeight = HWDefaultInputHeight;
    CGFloat offset = HWDefaultMargin;
    
    CGSize convertButtonSize = [self.convertButton sizeThatFits:CGSizeMake(actualBounds.size.width, inputHeight)];

    self.inputTextField.frame = CGRectMake(actualBounds.origin.x,
                                           actualBounds.origin.y,
                                           actualBounds.size.width - convertButtonSize.width - offset,
                                           inputHeight);
    
    self.convertButton.frame = CGRectMake(CGRectGetMaxX(actualBounds) - convertButtonSize.width,
                                          actualBounds.origin.y,
                                          convertButtonSize.width,
                                          inputHeight);
    
    CGSize resultLabelSize = [self.resultLabel sizeThatFits:CGSizeMake(actualBounds.size.width, CGFLOAT_MAX)];
    self.resultLabel.frame = CGRectMake(actualBounds.origin.x,
                                        CGRectGetMaxY(self.inputTextField.frame) + offset,
                                        actualBounds.size.width,
                                        resultLabelSize.height);
    
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGFloat height = self.contentInsets.top + HWDefaultInputHeight + self.contentInsets.bottom;
    
    if (self.resultLabel.text.length > 0) {
        height += [self.resultLabel sizeThatFits:CGSizeMake(size.width - self.contentInsets.left - self.contentInsets.right, CGFLOAT_MAX)].height + HWDefaultMargin;
    }
    return CGSizeMake(size.width, height);
}

@end
